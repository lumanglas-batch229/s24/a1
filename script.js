
// Exponent Operator

function gettingCube (num1, num2) {

let getCube = Math.pow (num1 ,3)
// Template Literals
console.log(`The cube of ${num1} is ${getCube}`);

}

gettingCube(2);



// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

let [houseNum, street, state, areaCode] = address;
console.log(`I live at ${houseNum} ${street}, ${state} ${areaCode}`)







// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
let {name,species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)

// Arrow Functions

let numbers = [1, 2, 3, 4, 5];

numbers.forEach ((x) => {
	console.log(x);
})



// Javascript Classes

class dog {
	constructor (name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
let dog1 = new dog ("Frankie","5","Miniature Dachshund")
let dog2 = new dog ("Jamies","2","Siberian Husky")

console.log(dog1);
console.log(dog2);
